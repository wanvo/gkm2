import {Component} from '@angular/core';
import { NgStyle } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

@Component({
  moduleId: module.id,
  selector: 'khuyen-mai',
  templateUrl: 'khuyen-mai.html',
  styleUrls: ['khuyen-mai.css'],
})
export class KhuyenMai {
  array = [
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-j-p-fashion-dong-gia-49k-trong-thang-10-2017.jpeg?itok=CK9SBzN5",content:"Khuyến mãi 2017 thời trang J-P Fashion đồng giá 49k trong tháng 10-2017",title:"Thời trang J-P Fashion"},
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-urban-nineteen-s-dong-gia-89k-trong-thang-10-2017.jpg?itok=8eXjpLfI",content:"Khuyến mãi 2017 thời trang Urban Nineteen's đồng giá 89k trong tháng 10-2017",title:"UrbanNineteen"},
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-j-p-fashion-dong-gia-49k-trong-thang-10-2017.jpeg?itok=CK9SBzN5",content:"Khuyến mãi 2017 thời trang J-P Fashion đồng giá 49k trong tháng 10-2017",title:"Thời trang J-P Fashion"},
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-urban-nineteen-s-dong-gia-89k-trong-thang-10-2017.jpg?itok=8eXjpLfI",content:"Khuyến mãi 2017 thời trang Urban Nineteen's đồng giá 89k trong tháng 10-2017",title:"UrbanNineteen"},
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-j-p-fashion-dong-gia-49k-trong-thang-10-2017.jpeg?itok=CK9SBzN5",content:"Khuyến mãi 2017 thời trang J-P Fashion đồng giá 49k trong tháng 10-2017",title:"Thời trang J-P Fashion"},
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-urban-nineteen-s-dong-gia-89k-trong-thang-10-2017.jpg?itok=8eXjpLfI",content:"Khuyến mãi 2017 thời trang Urban Nineteen's đồng giá 89k trong tháng 10-2017",title:"UrbanNineteen"},
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-j-p-fashion-dong-gia-49k-trong-thang-10-2017.jpeg?itok=CK9SBzN5",content:"Khuyến mãi 2017 thời trang J-P Fashion đồng giá 49k trong tháng 10-2017",title:"Thời trang J-P Fashion"},
    {url:"http://www.ghienkhuyenmai.com/sites/default/files/styles/thumb_w600/public/promotion/image/khuyen-mai-2017-thoi-trang-urban-nineteen-s-dong-gia-89k-trong-thang-10-2017.jpg?itok=8eXjpLfI",content:"Khuyến mãi 2017 thời trang Urban Nineteen's đồng giá 89k trong tháng 10-2017",title:"UrbanNineteen"},
    
  ];
}
