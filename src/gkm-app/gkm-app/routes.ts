import {Routes} from '@angular/router';
import {AccessibilityDemo} from '../a11y/a11y';
import {ACCESSIBILITY_DEMO_ROUTES} from '../a11y/routes';
import {GkmApp, Home} from './gkm-app';
import {KhuyenMai} from '../khuyen-mai/khuyen-mai';

export const GKM_APP_ROUTES: Routes = [
  {path: '', component: GkmApp, children: [
    {path: '', component: Home},
    {
      path: 'khuyen-mai',
      component: KhuyenMai,
      data: { title: 'Tất cả chương trình khuyến mãi | ghienkhuyenmai' }
    },
  ]}
];

export const ALL_ROUTES: Routes = [
  {path: '',  component: GkmApp, children: GKM_APP_ROUTES},
  {path: 'accessibility', component: AccessibilityDemo, children: ACCESSIBILITY_DEMO_ROUTES},
];
