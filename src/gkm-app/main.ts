import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {GkmAppModule} from './gkm-app-module';

platformBrowserDynamic().bootstrapModule(GkmAppModule);
