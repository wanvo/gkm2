import {task} from 'gulp';
import {tsBuildTask, copyTask, serverTask} from '../util/task_helpers';
import {join} from 'path';
import {
  buildConfig, copyFiles, buildScssTask, sequenceTask, watchFiles
} from 'material2-build-tools';
import {cdkPackage, materialPackage, momentAdapterPackage} from '../packages';

// These imports don't have any typings provided.
const firebaseTools = require('firebase-tools');

const {outputDir, packagesDir, projectDir} = buildConfig;

/** Path to the directory where all bundles live. */
const bundlesDir = join(outputDir, 'bundles');

const appDir = join(packagesDir, 'gkm-app');
const outDir = join(outputDir, 'packages', 'gkm-app');

/** Array of vendors that are required to serve the gkm-app. */
const appVendors = [
  '@angular', 'systemjs', 'zone.js', 'rxjs', 'hammerjs', 'core-js', 'web-animations-js', 'moment',
];

/** Glob that matches all required vendors for the gkm-app. */
const vendorGlob = `+(${appVendors.join('|')})/**/*.+(html|css|js|map)`;

/** Glob that matches all assets that need to be copied to the output. */
const assetsGlob = join(appDir, `**/*.+(html|css|svg)`);

task(':watch:gkmapp', () => {
  watchFiles(join(appDir, '**/*.ts'), [':build:gkmapp:ts']);
  watchFiles(join(appDir, '**/*.scss'), [':build:gkmapp:scss']);
  watchFiles(join(appDir, '**/*.html'), [':build:gkmapp:assets']);

  // Custom watchers for the CDK, Material and Moment adapter package. This is necessary because
  // we only want to build the package as a single entry-point (using the tests task).
  watchFiles(join(cdkPackage.sourceDir, '**/*'), ['cdk:build-no-bundles']);
  watchFiles(join(materialPackage.sourceDir, '**/!(*.scss)'), ['material:build-no-bundles']);
  watchFiles(join(materialPackage.sourceDir, '**/*.scss'), [':build:gkmapp:material-with-styles']);
  watchFiles(join(momentAdapterPackage.sourceDir, '**/*'),
      ['material-moment-adapter:build-no-bundles']);
});

/** Path to the gkm-app tsconfig file. */
const tsconfigPath = join(appDir, 'tsconfig-build.json');

task(':build:gkmapp:ts', tsBuildTask(tsconfigPath));
task(':build:gkmapp:scss', buildScssTask(outDir, appDir));
task(':build:gkmapp:assets', copyTask(assetsGlob, outDir));

task(':serve:gkmapp', serverTask(outDir, true));

// The themes for the gkm-app are built by using the SCSS mixins from Material.
// Therefore when SCSS files have been changed, the custom theme needs to be rebuilt.
task(':build:gkmapp:material-with-styles', sequenceTask(
  'material:build-no-bundles', ':build:gkmapp:scss'
));

task('build:gkmapp', sequenceTask(
  ['material-moment-adapter:build-no-bundles', ':build:gkmapp:assets'],
  [':build:gkmapp:scss', ':build:gkmapp:ts']
));

task('serve:gkmapp', ['build:gkmapp'], sequenceTask([':serve:gkmapp', ':watch:gkmapp']));

/** Task that copies all vendors into the gkm-app package. Allows hosting the app on firebase. */
task('stage-deploy:gkmapp', ['build:gkmapp'], () => {
  copyFiles(join(projectDir, 'node_modules'), vendorGlob, join(outDir, 'node_modules'));
  copyFiles(bundlesDir, '*.+(js|map)', join(outDir, 'dist/bundles'));
  copyFiles(cdkPackage.outputDir, '**/*.+(js|map)', join(outDir, 'dist/packages/cdk'));
  copyFiles(materialPackage.outputDir, '**/*.+(js|map)', join(outDir, 'dist/packages/material'));
  copyFiles(materialPackage.outputDir, '**/prebuilt/*.+(css|map)',
      join(outDir, 'dist/packages/material'));
});

/**
 * Task that deploys the gkm-app to Firebase. Firebase project will be the one that is
 * set for project directory using the Firebase CLI.
 */
task('deploy:gkmapp', ['stage-deploy:gkmapp'], () => {
  return firebaseTools.deploy({cwd: projectDir, only: 'hosting'})
    // Firebase tools opens a persistent websocket connection and the process will never exit.
    .then(() => { console.log('Successfully deployed the gkm-app to firebase'); process.exit(0); })
    .catch((err: any) => { console.log(err); process.exit(1); });
});
